class CPU {


	constructor(display, keyboard, audio) {
		this.display = display;
		this.keyboard = keyboard;
		this.audio = audio;

		// 4096 bytes of memory
		this.ram = new Uint8Array(4096);

		// 16 8-bit registers
		this.vRegister = new Uint8Array(16);

		// We start with 0
		this.indexRegister = 0;

		// Timers
		this.delayedTimer = 0;
		this.audioTmer = 0;

		// Program counter. Starts at 0x200 per documetnation.
		this.programCounter = 0x200;

		this.addressStack = new Array();

		this.pause = false;

		this.speed = window.globalSpeed;

    }
    

    loadFont() {
        
        // these are predefiend sprites which consist of 
        // the hex characters. Because of the limit they 
        // are realilly used. Each character consist of 5 groups of 8 bytes(sprite)
        // 
        // Here is how zero for example is 
        // **** 11110000 0xF0
        // *  * 10010000 0x90
        // *  * 10010000 0x90
        // *  * 10010000 0x90
        // **** 11110000 0xF0
        const characters = [
            0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
            0x20, 0x60, 0x20, 0x20, 0x70, // 1
            0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
            0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
            0x90, 0x90, 0xF0, 0x10, 0x10, // 4
            0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
            0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
            0xF0, 0x10, 0x20, 0x40, 0x40, // 7
            0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
            0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
            0xF0, 0x90, 0xF0, 0x90, 0x90, // A
            0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
            0xF0, 0x80, 0x80, 0x80, 0xF0, // C
            0xE0, 0x90, 0x90, 0x90, 0xE0, // D
            0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
            0xF0, 0x80, 0xF0, 0x80, 0x80  // F
        ];
    
        // As per documentation, fonts should be inserted at the beginning of 
        // the memory, that is from 0x000
        for (let i = 0; i < characters.length; i++) {
            this.ram[i] = characters[i];
        }
    }

    loadProgram(program) {
        for (let i = 0; i < program.length; i++) {
            this.ram[0x200 + i] = program[i];
        }
    }

    async loadRom(romName) {

        const romLocation = "./roms/" + romName;
        console.log("rom location:" + romLocation);
        const response = await fetch(romLocation);
        const buffer = await response.arrayBuffer();
        const uint8 = new Uint8Array(buffer);
        this.loadProgram(uint8);

    }

    cpuCycle() {
        for (let i = 0; i < window.globalSpeed; i++) {
            if (!this.pause) {
                let instruction = (this.ram[this.programCounter] << 8 | this.ram[this.programCounter + 1]);
                this.execute(instruction);
            }
        }

        if (!this.pause) {
            this.updateTimers();
        }
    
        this.playSound();
        this.display.show();
    }

    updateTimers() {
        if (this.delayedTimer > 0) {
            this.delayedTimer -= 1;
        }
    
        if (this.audioTmer > 0) {
            this.audioTmer -= 1;
        }
    }

    playSound() {
        if (this.audioTmer > 0) {
            this.audio.play(440);
        } else {
            this.audio.stop();
        }
    }

    execute(instruction) {

        // the program counter should point to the next instruction. 
        // we increase it by two because every instruction is 2 bytes
        this.programCounter += 2;

        // for example the instruction is 8xy1, to get the value of x 
        // we firstly AND it with 0x0F00, which would mean 0000 1111 0000 0000 
        // so the the value of x. But we need to do right bitwise operation 
        // for 8 bites, to get the value of x. We do the same for y. This is 
        // not used in all instructions, but it is used often enough so it makes 
        // sense do it here, althought it won't be used for all instrutions. 
        let x = (instruction & 0x0F00) >> 8;

        // See the logic for x, it is the same but we should just do rightshift by 4 bits
        let y = (instruction & 0x00F0) >> 4;

        // we look here at the first number
        switch (instruction & 0xF000) {
            // if the first number is 0
            case 0x0000:
                switch (instruction) {

                    // 00E0 - CLS 
                    // Clear the display.
                    case 0x00E0:
                        this.display.clearDisplay();
                        break;
                    
                    // 00EE - RET
                    // Return from a subroutine.
                    case 0x00EE:
                        this.programCounter = this.addressStack.pop();
                        break;
                }
        
                break;
            
            // if the first number is 1
            // 1nnn - JP addr
            // Jump to location nnn.
            case 0x1000:
                this.programCounter = (instruction & 0xFFF);
                break;

            // if the first number is 2 of the instruction
            // 2nnn - CALL addr
            // Call subroutine at nnn.
            case 0x2000:
                // 16 is the capacity of the stack
                if(this.addressStack.length === 16) {
                    this.pause = true;
                    throw new Error('Stack overflow')
                }
                this.addressStack.push(this.programCounter);
                var nnn = instruction & 0xFFF; 
                this.programCounter = nnn;
                break;

            // if the first number is 3 of the instruction 
            // 3xkk - SE Vx, byte
            // Skip next instruction if Vx = kk.    
            case 0x3000:
                var kk = instruction & 0xFF;
                if (this.vRegister[x] === (kk)) {
                    this.programCounter += 2;
                }
                break;

            // if the first number if 4 of the instruction    
            // 4xkk - SNE Vx, byte
            // Skip next instruction if Vx != kk.
            case 0x4000:
                var kk = instruction & 0xFF
                if (this.vRegister[x] !== kk) {
                    this.programCounter += 2;
                }
                break;

            // if the first number if 5 of the instruction 
            // 5xy0 - SE Vx, Vy
            // Skip next instruction if Vx = Vy.    
            case 0x5000:
                if (this.vRegister[x] === this.vRegister[y]) {
                    this.programCounter += 2;
                }
                break;

            // if the first number is 6 of the instruction
            // 6xkk - LD Vx, byte
            // Set Vx = kk.
            case 0x6000:
                var kk = instruction & 0xFF
                this.vRegister[x] = kk;
                break;

            // if the first number is 7 of the instruction 
            // 7xkk - ADD Vx, byte
            // Set Vx = Vx + kk.    
            case 0x7000:
                this.vRegister[x] += (instruction & 0xFF);
                break;

            // if the first number is 8
            case 0x8000:
                // the last digit 
                switch (instruction & 0xF) {

                    // if the last digit is 0
                    // 8xy0 - LD Vx, Vy
                    // Set Vx = Vy.
                    case 0x0:
                        this.vRegister[x] = this.vRegister[y];
                        break;

                    // if the last digit is 1
                    // 8xy1 - OR Vx, Vy
                    // Set Vx = Vx OR Vy.    
                    case 0x1:
                        this.vRegister[x] = this.vRegister[x] | this.vRegister[y]
                        break;

                    // if the last digit is 2
                    // 8xy2 - AND Vx, Vy
                    // Set Vx = Vx AND Vy.    
                    case 0x2:
                        this.vRegister[x] = this.vRegister[x] & this.vRegister[y]
                        break;

                    // if the last digit is 3
                    // 8xy3 - XOR Vx, Vy
                    // Set Vx = Vx XOR Vy.    
                    case 0x3:
                        this.vRegister[x] =  this.vRegister[x] ^ this.vRegister[y]
                        break;

                    // if the last digit s 4
                    // 8xy4 - ADD Vx, Vy
                    // Set Vx = Vx + Vy, set VF = carry.
                    case 0x4:
                        this.vRegister[x] = this.vRegister[x] + this.vRegister[y]

                        this.vRegister[0xF] = 0;

                        if (this.vRegister[x] > 0xFF) {
                            this.vRegister[0xF] = 1;
                        }

                        break;

                    // if the last digit is 5
                    // 8xy5 - SUB Vx, Vy
                    // Set Vx = Vx - Vy, set VF = NOT borrow.
                    // If Vx > Vy, then VF is set to 1, otherwise 0. 
                    // Then Vy is subtracted from Vx, and the results stored in Vx.   
                    case 0x5:
                        this.vRegister[0xF] = 0;

                        if (this.vRegister[x] > this.vRegister[y]) {
                            this.vRegister[0xF] = 1;
                        }

                        this.vRegister[x] -= this.vRegister[y];
                        break;

                    // if the last digit is 6
                    // 8xy6 - SHR Vx {, Vy}
                    // Set Vx = Vx SHR 1. 
                    // Basicually it does right shift     
                    case 0x6:
                        this.vRegister[0xF] = (this.vRegister[x] & 0x1);

                        this.vRegister[x] >>= 1;
                        break;

                
                    // if hte last digit is 7
                    // 8xy7 - SUBN Vx, Vy
                    // Set Vx = Vy - Vx, set VF = NOT borrow.
                    // If Vy > Vx, then VF is set to 1, otherwise 0. Then Vx is subtracted from Vy, and the results stored in Vx.
                    case 0x7:
                        this.vRegister[0xF] = 0;

                        if (this.vRegister[y] > this.vRegister[x]) {
                            this.vRegister[0xF] = 1;
                        }

                        this.vRegister[x] = this.vRegister[y] - this.vRegister[x];
                        break;

                    // if the last digit is E
                    // 8xyE - SHL Vx {, Vy}
                    // Set Vx = Vx SHL 1.    
                    case 0xE:
                        // 0x80 is 1000000, so we get the most  significant bit
                        this.vRegister[0xF] = (this.vRegister[x] & 0x80);
                        // the original instruction is multiply by 2 
                        // so we do a left shift 
                        this.vRegister[x] <<= 1;
                        break;
                }
        
                break;
            
            // if the instruction starts with 9    
            // 9xy0 - SNE Vx, Vy
            // Skip next instruction if Vx != Vy.
            case 0x9000:
                if (this.vRegister[x] !== this.vRegister[y]) {
                    this.programCounter += 2;
                }
                break;
            
            // if the instruction starts with A(remember it is a hex digit)    
            // Annn - LD I, addr    
            // Set I = nnn
            case 0xA000:
                var nnn = instruction & 0xFFF
                this.indexRegister = nnn;
                break;

            // if the instruction starts with B
            // BNNN
            // Jump to location nnn + V0.
            // The program counter is set to nnn plus the value of V0  
          
            case 0xB000:
                var nnn = instruction & 0xFFF;
                this.programCounter = nnn + this.vRegister[0];
                break;

            // if the instruction starts with C
            // Cxkk - RND Vx, byte
            // Set Vx = random byte AND kk.  
            case 0xC000:
                // the random value should be in range from 0 to 255(inclusive, in decimal system)
                // 0xFF = 256 and Math.random is inclusive of 0, but not 1.
                var randomValue = Math.floor(Math.random() * 0xFF);

                var kk = instruction & 0xFF
                this.vRegister[x] = randomValue & kk;
                break;

            // if the instruction starts with D
            // Dxyn - DRW Vx, Vy, nibble
            // Display n-byte sprite starting at memory location I at (Vx, Vy), set VF = collision.    
            case 0xD000:
    
                var n = instruction & 0xF;

                // if there is an overlap of pixels this will change to 1 later in the code
                this.vRegister[0xF] = 0;

                for (let i = 0; i < n; i++) {
                    let sprite = this.ram[this.indexRegister + i];

                    for (let j = 0; j < 8; j++) {
                        
                        if ((sprite & 0x80) != 0) {
                            // setPixel return 1 when pixel was aflipped
                            if (this.display.setPixel(this.vRegister[x] + j, this.vRegister[y] + i)) {
                                this.vRegister[0xF] = 1;
                            }
                        }

                        // Shift the sprite left by 1 bit.
                        sprite <<= 1;
                    }
                }
                break;

            // if it starts with an E   
            case 0xE000:
                switch (instruction & 0xFF) {
                     // Ex9E - SKP Vx
                     //Skip next instruction if key with the value of Vx is pressed 
                    case 0x9E:
                        if (this.keyboard.isKeyPressed(this.vRegister[x])) {
                            this.programCounter += 2;
                        }
                        break;

                    // ExA1 - SKNP Vx
                    // Skip next instruction if key with the value of Vx is not pressed.     
                    // same as before but only if it not pressed
                    case 0xA1:
                        if (!this.keyboard.isKeyPressed(this.vRegister[x])) {
                            this.programCounter += 2;
                        }
                        break;
                }
        
                break;

            // if it starts with F    
            case 0xF000:
                switch (instruction & 0xFF) {
                    // Fx07 - LD Vx, DT
                    // Set Vx = delay timer value.
                    case 0x07:
                        this.vRegister[x] = this.delayedTimer;
                        break;
                    // Fx0A - LD Vx, K
                    // Wait for a key press, store the value of the key in Vx.    
                    case 0x0A:
                        this.pause = true;

                        // this function is called in the Keyboard class 
                        this.keyboard.onKeyPress = function(key) {
                            this.v[x] = key;
                            this.paused = false;
                        }.bind(this);
                        break;
                    
                    // Fx15 - LD DT, Vx
                    // Set delay timer = Vx.
                    case 0x15:
                        this.delayedTimer = this.vRegister[x];
                        break;

                    // Fx18 - LD ST, Vx
                    // Set sound timer = Vx.
                    case 0x18:
                        this.audioTmer = this.vRegister[x];
                        break;
                    
                    // Fx1E - ADD I, Vx
                    // Set I = I + Vx.    
                    case 0x1E:
                        this.indexRegister += this.vRegister[x];
                        break;

                    // Fx29 - LD F, Vx
                    // Set I = location of sprite for digit Vx.    
                    case 0x29:
                        this.indexRegister = this.vRegister[x] * 0x5;
                        break;

                    // Fx33 - LD B, Vx
                    // Store BCD representation of Vx in memory locations I, I+1, and I+2.    
                    case 0x33:
                       
                        var valueInXRegister = this.vRegister[x];

                        const a = Math.floor(valueInXRegister / 100)
                        valueInXRegister = valueInXRegister - a * 100 
                        const b = Math.floor(valueInXRegister / 10) 
                        valueInXRegister = valueInXRegister - b * 10 
                        const c = Math.floor(valueInXRegister);

                        this.ram[this.indexRegister] = a
                        this.ram[this.indexRegister + 1] = b
                        this.ram[this.indexRegister + 2] = c
                        break;

                    // Fx55 - LD [I], Vx    
                    // Store registers V0 through Vx in memory starting at location I.
                    case 0x55:
                        for (let registerIndex = 0; registerIndex <= x; registerIndex++) {
                            this.ram[this.indexRegister + registerIndex] = this.vRegister[registerIndex];
                        }
                        break;

                    // Fx65 - LD Vx, [I]
                    // Read registers V0 through Vx from memory starting at location I.     
                    case 0x65:
                        for (let registerIndex = 0; registerIndex <= x; registerIndex++) {
                            this.vRegister[registerIndex] = this.ram[this.indexRegister + registerIndex];
                        }
                        break;
                }
        
                break;
        
            default:
                throw new Error('Invalid instruction: ' + instruction);
        }
    }
}

module.exports = {
    CPU
}