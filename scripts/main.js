const Display = require('./display.js').Display
const Keyboard = require('./keyboard.js').Keyboard
const Audio = require('./audio').Audio
const CPU = require('./cpu.js').CPU

let display = new Display(10);
let keyboard = new Keyboard();
let audio = new Audio();
let cpu = new CPU(display, keyboard, audio);


// frames are not defined in the documentation, 
// but the better devices that run CHIp-8 usually 
// had 60Hz, which means that the screen is refreshed 
// 60 times per seconds.
const FRAMES_PER_SECOND = 60;
const MILLISECONDS_IN_A_SECOND = 1000;
let framesPerSecondInterval;

function init() {

	framesPerSecondInterval = MILLISECONDS_IN_A_SECOND / FRAMES_PER_SECOND;

	// this is is also not specified in the documentation, but depends on the 
	// implementation of the underlaying hardware. The logic used here is that 
	// the CPU was usually from 1Mhz to 2 Mhz(COSMAC Vip for example, which as the name
	// suggest was considered a good computer had 1.76Mhz), The screen is refreshed every 
	// 16.7 millisecond, and on every refresh around 10 instruction are executed so this value 
	// is chosen to simulate behaviour which is similar to the hardware
	window.globalSpeed = 10;

	cpu.loadFont();
	cpu.loadRom('TANK');
	document.getElementById("selectGame").addEventListener("change", changeRom);
	document.getElementById("speed").addEventListener("change", changeSpeed)
	setInterval(function () {
		cpu.cpuCycle();
	}, framesPerSecondInterval);
}


function changeSpeed() {
    const speed = event.target.value;
    console.log("speed:" + speed);
    window.globalSpeed = speed;
}

function changeRom() {
	audio.stop();
    display = new Display(10);
    keyboard = new Keyboard();
    audio = new Audio();
    cpu = new CPU(display, keyboard, audio);
    const rom = event.target.value;
    cpu.loadRom(rom);
}


init();

document.getElementById("selectGame").addEventListener("change", changeRom);
document.getElementById("speed").addEventListener("change", changeSpeed)