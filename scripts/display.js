class Display {
    
    constructor(scale) {
        this.columns = 64;
        this.rows = 32;

        this.canvasScale = scale;

        this.canvas = document.querySelector('canvas');
        this.canvasContext = this.canvas.getContext('2d');

        // the original screen size is 32x64, which would look
        // tiny on modern displays, so we multiply it
        this.canvas.width = this.columns * this.canvasScale;
        this.canvas.height = this.rows * this.canvasScale;

        this.display = new Array(this.columns * this.rows);
    }

    setPixel(x, y) {
        if (x > this.columns) {
            x -= this.columns;
        } else if (x < 0) {
            x += this.columns;
        }
        
        if (y > this.rows) {
            y -= this.rows;
        } else if (y < 0) {
            y += this.rows;
        }


        let pixelLocation = x + (y * this.columns);

        this.display[pixelLocation] ^= 1;

        return !this.display[pixelLocation];
    }

    clearDisplay() {
        this.display = new Array(this.columns * this.rows);
    }

    show() {
        
        
        this.canvasContext.clearRect(0, 0, this.canvas.width, this.canvas.height);
    
        for (let i = 0; i < this.columns * this.rows; i++) {

         
            var x = (i % this.columns) * this.canvasScale;
    
     
            var y = Math.floor(i / this.columns) * this.canvasScale;
    
            
            if (this.display[i]) {
                // #000 is black
                this.canvasContext.fillStyle = '#000';
                this.canvasContext.fillRect(x, y, this.canvasScale, this.canvasScale);
            }
        }
    }
}

module.exports = {
    Display: Display
}