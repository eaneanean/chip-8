class Keyboard {

    constructor() {

        this.KEYS = {
			49: 0x1, // 1 was pressed
			50: 0x2, // 2 was pressed
			51: 0x3, // 3 was pressed
			52: 0xc, // 4 was pressed
			81: 0x4, // Q was pressed....
			87: 0x5, // W
			69: 0x6, // E
			82: 0xD, // R
			65: 0x7, // A
			83: 0x8, // S
			68: 0x9, // D
			70: 0xE, // F
			90: 0xA, // Z
			88: 0x0, // X
			67: 0xB, // C
			86: 0xF  // V
		}

        this.pressed = [];
        this.onKeyPress = null;


        window.addEventListener('keydown', this.onKeyDown.bind(this), false);
		window.addEventListener('keyup', this.onKeyUp.bind(this), false);
    }

    isKeyPressed(keyCode) {
        return this.pressed[keyCode];
    }

    onKeyDown(event) {
        let key = this.KEYS[event.which];
        this.pressed[key] = true;
    
        // Check whether key was pressed
        if (this.onKeyPress !== null && key) {
            this.onKeyPress(parseInt(key));
            this.onKeyPress = null;
        }
    }

    onKeyUp(event) {
        let key = this.KEYS[event.which];
        this.pressed[key] = false;
    }
}

module.exports = {
    Keyboard
}