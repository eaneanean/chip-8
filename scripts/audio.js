class Audio {
	constructor() {
		const AudioContext = window.AudioContext || window.webkitAudioContext;

		this.localAudioContext = new AudioContext();

    
		this.gain = this.localAudioContext.createGain();
		this.finish = this.localAudioContext.destination;

        
		this.gain.connect(this.finish);
    }
    
    play(frequency) {
        if (this.localAudioContext && !this.oscillation) {
            this.oscillation = this.localAudioContext.createOscillator();
    
            // Set the frequency
            this.oscillation.frequency.setValueAtTime(frequency || 400, this.localAudioContext.currentTime);
    
    
            this.oscillation.connect(this.gain);
            this.oscillation.start();
        }
    }

    // The oscilator is removed, because it is not possible to 
    // start the oscilator more that once, that is you cannot call
    // the start() method more than once.
    stop() {
        if (this.oscillation) {
            this.oscillation.stop();
            this.oscillation.disconnect();
            this.oscillation = null;
        }
    }
}

module.exports = {
    Audio: Audio
}

